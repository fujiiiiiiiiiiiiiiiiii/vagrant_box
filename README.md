# vagrantbox centos6.6 minimal x64

## box list

### centos-6.6-x86_64.box
base box:https://github.com/tommy-muehle/puppet-vagrant-boxes/releases/download/1.0.0/centos-6.6-x86_64.box

#### installed
- minimal
- GuestAdditions 5.0
- ansible 2.2
